package com.allstate.labs;

public  class Account {

    private double balance;
    private String name;
    private static double interestRate=0.1;

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public  void addInterest(){
    /* double interest=this.balance*interestRate;
        this.balance+=interest; */
    }
    
    public boolean withdraw(){
        return withdraw(20);
    }
    public boolean withdraw(double amt){
        if(this.balance>=amt){
            this.balance-=amt;
            System.out.println("Amount "+amt+ " has been deducted from your account "+this.name);
                    return true;
        }
        System.out.println("Insufficient amount to withdraw from the account "+this.name);
        return false;
    }

	

    public Account(double balance, String name) {
        this.balance = balance;
        this.name = name;
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }


    
    
}
