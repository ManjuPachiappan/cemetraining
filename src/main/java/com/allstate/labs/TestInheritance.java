package com.allstate.labs;

public class TestInheritance {
   public static void main(String[] args) {
    Account[] arrayOfAccounts=new Account[3];
    double[] amounts={2, 4,6};
    arrayOfAccounts[0]=new Account(amounts[0], "Manju");
        arrayOfAccounts[1]=new SavingsAccount(amounts[1], "Durga");
        arrayOfAccounts[2]=new CurrentAccount(amounts[2], "Radha");
        for(int i=0;i<arrayOfAccounts.length;i++){
        System.out.println( "Name of the account holder "+arrayOfAccounts[i].getName()+" and balance is " +arrayOfAccounts[i].getBalance()); 
          arrayOfAccounts[i].addInterest();
        System.out.println( "Name of the account holder "+arrayOfAccounts[i].getName()+" and balance after interest added " +arrayOfAccounts[i].getBalance()); 
    }
    
}
}
